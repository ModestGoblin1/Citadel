//
//  CitadelApp.swift
//  Shared
//
//  Created by Thomas Sikma on 2021-01-20.
//

import SwiftUI

@main
struct CitadelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
