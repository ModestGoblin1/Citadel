//
//  ContentView.swift
//  Shared
//
//  Created by Thomas Sikma on 2021-01-20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, home!").padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
