# Citadel

Openly creating, implempenting and [discussing](https://github.com/ModestGoblin/Citadel/discussions) **Smart Home** Design and Development projects!(e.g.,automated lights, thermostats, door locks and so much more) 

Things to familiarize ourselves with! Why not:
- the HomeKit framework
- HomeKit Accessory Protocol (HAP)
- Augmented Reality 



Open Task list:
- [ ] Get noticed 🧘‍♀️
- [ ] Organize Task list(s)*
- [ ] Funding for full-time Open-source Development and Design






Closed Task list:

<!-- - [x] **--> 
- [x] *Apple HomePod mini*
- [x] *Apple Developer Program membership*
- [x] *start*
















<!--\Community Donation link?--> 


[HomeKit Accessory Development Kit ADK](https://github.com/ModestGoblin/HomeKitADK)

*The information provided in the HomeKit Accessory Protocol Specification (Non-Commercial Version) describes how to implement HAP in an accessory that you create for non-commercial use and that will not be distributed or sold.*
